﻿let data = window.location.hash;

if (data) {

	let url = new URL(data.substr(1));

	if (url.protocol === window.location.protocol
		&& url.host === window.location.host) {
		let section = document.getElementById('urlResult');
		section.innerHTML = `<a id="link" href="${url}">${url}</a>`;
	}
}