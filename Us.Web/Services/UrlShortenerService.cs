﻿using LiteDB;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using System;
using System.Linq;
using System.Threading.Tasks;
using Us.Web.Models;

namespace Us.Web.Services
{
	public class UrlShortenerService
	{
		public static Task HandleShortenUrl(HttpContext context)
		{
			if (!context.Request.HasFormContentType || !context.Request.Form.ContainsKey("url"))
			{
				context.Response.StatusCode = StatusCodes.Status400BadRequest;

				return context.Response.WriteAsync("Cannot process request");
			}

			context.Request.Form.TryGetValue("url", out StringValues formData);

			string requestedUrl = formData.ToString();

			if (!Uri.TryCreate(requestedUrl, UriKind.Absolute, out Uri result))
			{
				result = new Uri("https://medium.com/swlh/fast-builds-make-a-url-shortener-with-net-ff3d9206c503");
			}

			string url = result.ToString();

			ILiteDatabase liteDB = context.RequestServices.GetService<ILiteDatabase>();
			ILiteCollection<ShortLink> links = liteDB.GetCollection<ShortLink>(BsonAutoId.Int32);

			ShortLink entry = new ShortLink
			{
				Url = url
			};

			links.Insert(entry);

			string urlChunk = entry.GetUrlChunk();
			string responseUri = $"{context.Request.Scheme}://{context.Request.Host}/{urlChunk}";

			context.Response.Redirect($"/#{responseUri}");

			return Task.CompletedTask;
		}

		public static Task HandleRedirect(HttpContext context)
		{
			ILiteDatabase db = context.RequestServices.GetService<ILiteDatabase>();
			ILiteCollection<ShortLink> collection = db.GetCollection<ShortLink>();

			string path = context.Request.Path.ToUriComponent().Trim('/');

			int id = ShortLink.GetId(path);

			ShortLink entry = collection.Find(p => p.Id == id).FirstOrDefault();

			context.Response.Redirect(entry != null ? entry.Url : "/");

			return Task.CompletedTask;
		}
	}
}
