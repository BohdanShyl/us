using LiteDB;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Us.Web.Services;

namespace Us.Web
{
	public class Startup
	{
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddRouting();
			services.AddSingleton<ILiteDatabase, LiteDatabase>(_ => new LiteDatabase("short-links.db"));
		}

		public void Configure(IApplicationBuilder app)
		{
			app.UseDefaultFiles();
			app.UseStaticFiles();
			app.UseRouting();
			app.UseEndpoints((endpoints) =>
			{
				endpoints.MapPost("/shorten", UrlShortenerService.HandleShortenUrl);
				endpoints.MapFallback(UrlShortenerService.HandleRedirect);
			});
		}
	}
}
